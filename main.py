import sys
from flask import Flask
from flask_restplus import Resource, Api, reqparse, fields
from StringsComparator import StringsComparator


# Definition of the API
app = Flask(__name__)
api = Api(app = app, title = 'Technical Test API')
queries_model = api.model('API', 
                          {'queries': fields.String(required    = True, 
                                                    description = 'This is the list of strings that will be compared to the reference string',
                                                    example     = 'ab,abc,bc')})



name_space = api.namespace(name        = 'Strings Comparator', 
                           description = 'Returns a JSON that associates each element of the argument "queries" to its number of occurence in a hidden list of strings.')

@name_space.route('/')
class SparseArray(Resource):

    @api.expect(queries_model)
    def post(self):
        # Retrievial of the POST values
        parser = reqparse.RequestParser()
        parser.add_argument('queries', required=True)

        # Counts the occurences of every element of queries in hidden_strings
        queries            = parser.parse_args()['queries'].split(',') # List of Strings
        queries_occurences = hidden_strings_wrapped.count_occurence_of(queries)

        return {'queries_occurences': queries_occurences}, 200


    

if __name__ == '__main__':

    # Stores the python input into an instance of StringsComparator (a String Wrapper)
    hidden_strings         = sys.argv[1].split(',') # List of Strings
    hidden_strings_wrapped = StringsComparator(hidden_strings)

    
    app.run(host='0.0.0.0', port=5000)

