# Technical Test 

## Objectif 
This program runs a flask API that solves a problem inspired by [this](https://www.hackerrank.com/challenges/sparse-arrays/problem), inside a docker container.

## Description of the Problem
As explained in [this](https://www.hackerrank.com/challenges/sparse-arrays/problem) :
*Given a collection of input strings and a collection of query strings. For each query string, determine how many times it occurs in the list of input strings.*

#### For Example
Given `hidden_strings = ['ab','ab','abc']` and `query_strings = ['ab','abc','bc']`, there are **2 instances of ab**, **1 instance of abc** and **0 instance of bc** in **hidden_strings**, so the solution is ` queries_occurences = {"ab": 2, abc": 1, "bc": 0}`.

### Notes :
**hidden_strings** is a constant defined when constructing the docker container. This constant is hidden to the user of the API.\
**query_strings** will be defined by the query the user sends to the API.\
**queries_occurences** is the output of the API. It counts the occurence of each element of **query_strings** in **hidden_strings**.

## Files
**main.py**: Defines and launches the API.\
**StringsComparator.py**: Includes the class that is used to solve the problem.\
**Dockerfile**: includes the docker instructions.

## Installation

If needed, follow the official instructions to install [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) and [docker](https://docs.docker.com/get-docker/) on your device.

#### Clone the git project
Clone this gitlab project : \
` $ git clone https://gitlab.com/Louis-Dupont/technical-test.git `\

### Move to this new folder
` $ cd technical-test`

#### Build a docker image 

` $ docker build . -t test_mdm `\
Here the name is *test_mdm* but it could be anything.

#### Start a container built upon this docker image 
` $ docker run -p 5000:5000 -t test_mdm args `\
Replace **args** by the list of strings you want to hide in your API, in the format :
`string_1,string_2, ...,string_n`


For instance: \
` $ docker run -p 5000:5000 -t test_mdm ab,ab,abc `

### Done !
You should now have a flask API running inside a docker container.

## Use the API
You can now interact with the API using the swagger UI, by just clicking on [http://localhost:5000/](http://localhost:5000/).

Select **Strings Comparator**. You can send your own request using **POST** by clicking on *Try it out*.\
You can modify **queries** as you want as long as it keeps the format :
```
{
  "queries": "string_1,string_2, ...,string_n"
}
```

For instance:
```
{
  "queries": "ab,abc,bc"
}
```

The output will be sent in JSON format.


## Example

**Git** \
` $ git clone https://gitlab.com/Louis-Dupont/technical-test.git `

**DOCKER**\
` $ cd technical-test`\
` $ docker build . -t test_mdm `\
` $ docker run -p 5000:5000 -t test_mdm ab,ab,abc `

**POST** 
```
{
  "queries": "ab,abc,bc"
}
```

**SERVER RESPONSE**

```
{
  "queries_occurences": {
    "ab": 2,
    "abc": 1,
    "bc": 0
  }
}
```

```