FROM python:3.7

RUN pip install flask==1.1.2
RUN pip install flask_restplus==0.13.0
RUN pip install Werkzeug==0.16.1

ADD main.py .
ADD StringsComparator.py .

EXPOSE 5000

ENTRYPOINT [ "python", "main.py" ]