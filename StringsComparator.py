class StringsComparator:
    """
        Wrappes a list of Strings to compare this list of strings to others
        Can be used to count the occurence of each element of another list of strings
        
        
        Attributes
        ------------
        strings : list(str)
            The list of strings that is wrapped
            

        Methods
        ------------
        count_occurence_of(queries: list(str)) -> dict
            Returns a dictionary that associates each element of the argument "queries" to its number of occurence in the attribute "strings"
            Example: given self.strings = ['ab','ab','abc'] and queries = ['ab','abc','bc']
                     returns queries_occurences = {"ab": 2, abc": 1, "bc": 0}
                     
            
    """
    
    def __init__(self, strings):
        self.strings = strings
        
    def count_occurence_of(self, queries):
        queries_occurence = dict() 
        for query in queries:
            count_query_occurence = 0
            for string in self.strings:
                if query == string:
                    count_query_occurence += 1
            queries_occurence[query] = count_query_occurence
        return queries_occurence
